![logo_small.png](https://bitbucket.org/repo/nqRLyM/images/1640048026-logo_small.png)
### Prediction-based Annotation for Crowded Environments ##

A web based annotation tool to create ground truth for datasets related to visual surveillance and behavior understanding. 
The system persistence is based on a relational database and the user interface is designed using HTML5, Javascript and CSS. 
Our tool can easily manage datasets with multiple cameras. It allows annotating a person location in the image, its identity, its body and head gaze, as well as a potential occlusion or group membership.


CONTENTS:

- index.php: home page of PACE
- /gt-making: contains the integrated environment for annotating
- export.php: script to export the entire database in the CSV format
- legend.html: contains the keyboard shortcuts
- settings.php: contains all settings used for annotating (e.g. database connection, users, proposals based on the propagation, cameras calibration)
- /frames: contains the frames to be annotated with PACE
- /database: scripts to generate and popolate the database
- /img/avatars: contains one representative image for each annotated people
- /css: style sheets
- /fonts: font files
- /js: javascript files
- /php: contains api.php with functions to run queries and connection.ini where are stored the parameters for the connection with the database (it is will be automatically generated on the first start up of the system)
- Readme: The file you are reading now :)...
- contributors.txt: peoples who have contributed to implement PACE


[*setup.php*]

This file is called the first time you're starting PACE. This component allows to set some parameters of PACE:*config.conf* file is 
- [Database Connection]: you can specify the user,password and host to connect PACE with the database.
 **Note: The user specified must be granted privileges on this database. We also assume MySQL is listening on default port 3306**
- [Cameras]: allows to insert the frame to be annotated 
- [Data]: allows to import annotations data (optionally) 
- [Users]: allows to insert the users for annotating


QUICK START INSTRUCTIONS:

1. Put PACE code on web server e.g. by cloning the repo. **Note: ensure PACE has write privileges** 
2. You should be able to start annotating opening a Web browser at: http://localhost/PACE/

[online PACE]

You can try PACE at http://150.217.35.152/PACE, with Name=Guest and Password=Guest